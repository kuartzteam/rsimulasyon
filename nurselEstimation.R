nurselEstimation <- function(qh = 1){
  
  denklemMatrisi = matrix(0,nrow=3,ncol=4)
  
  N = nrow(pop1) + nrow(pop2) + nrow(pop3)
  
  tabakaAgirliklari = c(nrow(pop1) / N, nrow(pop2) / N, nrow(pop3) / N) ## vektör --> [ Nh/N , Nh/N , Nh/N  ]
  
  # fsamp1, fsamp2, fsamp3
  # dsamp1, dsamp2, dsamp3
  
  # örneklemlerin bağımlı değişken ortalamaları
  dSampleYMeans = c( mean(dsamp1[,1]), mean(dsamp2[,1]), mean(dsamp3[,1]) )
  
  ## örneklemlerin yardımcı değişken ortalamaladır
  fSampleMeans = c( mean(fsamp1[,2]), mean(fsamp2[,2]), mean(fsamp3[,2]) )
  dSampleMeans = c( mean(dsamp1[,2]), mean(dsamp2[,2]), mean(dsamp3[,2]) )
  
  ## örneklemin yardımcı değişken varyanslarıdır.
  fSampleVar = c( var(fsamp1[,2]), var(fsamp2[,2]), var(fsamp3[,2]) )
  dSampleVar = c( var(dsamp1[,2]), var(dsamp2[,2]), var(dsamp3[,2]) )
  
  for(i in 1:3) {
    denklemMatrisi[1,1] = denklemMatrisi[1,1] + tabakaAgirliklari[i] * ( fSampleMeans[i] - dSampleMeans[i] )
    denklemMatrisi[1,2] = denklemMatrisi[1,2] + tabakaAgirliklari[i] * qh * dSampleMeans[i]^2
    denklemMatrisi[1,3] = denklemMatrisi[1,3] + tabakaAgirliklari[i] * qh * dSampleMeans[i] * dSampleVar[i]
    denklemMatrisi[1,4] = denklemMatrisi[1,4] + tabakaAgirliklari[i] * qh * dSampleMeans[i]
    
    denklemMatrisi[2,1] = denklemMatrisi[2,1] + tabakaAgirliklari[i] * ( fSampleVar[i] - dSampleVar[i] )
    denklemMatrisi[2,2] = denklemMatrisi[1,3] ## simetrik bir matris oldugu icin aynı.
    denklemMatrisi[2,3] = denklemMatrisi[2,3] + tabakaAgirliklari[i] * qh * dSampleVar[i]^2
    denklemMatrisi[2,4] = denklemMatrisi[2,4] + tabakaAgirliklari[i] * qh * dSampleVar[i]
    
    
    denklemMatrisi[3,1] = 0 ## clement harici sıfır
    denklemMatrisi[3,2] = denklemMatrisi[1,4]
    denklemMatrisi[3,3] = denklemMatrisi[2,4]
    denklemMatrisi[3,4] = tabakaAgirliklari[i] * qh
  }
  
  lambdaVektor = formulOn(denklemMatrisi)
  
  nurselOmega = 0
  nurselTahmin = 0
  
  for(i in 1:3) {
    nurselOmega = tabakaAgirliklari[i] + tabakaAgirliklari[i] * qh * ( lambdaVektor[1] *  dSampleMeans[i] + lambdaVektor[2] * dSampleVar[i] + lambdaVektor[3] )
    nurselTahmin = nurselTahmin + nurselOmega * dSampleYMeans[i]
  }
  
  return (nurselTahmin)
  
}
